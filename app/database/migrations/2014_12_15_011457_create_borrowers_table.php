<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('borrowers', function($table)
		    {
		        $table->increments('number')->unique();
		        $table->string('fname')->nullable();
		        $table->string('lname')->nullable();
		        $table->string('type')->nullable();
		        $table->string('address')->nullable();
		        $table->string('department')->nullable();
		        $table->string('section')->nullable();
		        $table->string('year')->nullable();
		        $table->string('contact')->nullable();
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('borrowers');
	}

}
