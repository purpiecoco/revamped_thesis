<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('returns', function($table)
		    {
		        $table->increments('number')->unique();
		        $table->string('booknumber')->nullable();
		        $table->string('borrowernumber')->nullable();
		        $table->date('date_borrowed')->nullable();
		        $table->date('expected_return_date')->nullable();
		        $table->date('actual_date_returned')->nullable();
		        $table->integer('payments')->nullable();
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('returns');
	}

}
