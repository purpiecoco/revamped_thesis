<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('books', function($table)
		    {
		        $table->increments('number')->unique();
		        $table->string('callnumber')->nullable();
		        $table->string('title')->nullable();
		        $table->string('author')->nullable();
		        $table->string('subauthor')->nullable();
		        $table->string('publisher')->nullable();
		        $table->string('quantity')->nullable();
		        $table->string('genre')->nullable();
		        $table->string('description')->nullable();
		        $table->float('price')->nullable();
		        $table->date('date')->nullable();
		        $table->string('archive_status')->nullable();
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('books');
	}

}
