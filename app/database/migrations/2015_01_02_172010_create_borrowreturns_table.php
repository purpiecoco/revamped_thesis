<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowreturnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('borrowreturns', function($table)
		    {
		        $table->increments('number')->unique();
		        $table->string('booknumber')->nullable();
		        $table->string('borrowernumber')->nullable();
		        $table->date('date_borrowed')->nullable();
		        $table->date('expected_return_date')->nullable();
		    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('borrowreturns');
	}

}
