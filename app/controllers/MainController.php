<?php

class MainController extends \BaseController {
	public function login(){
		$username = Input::get('username');
		$password = Input::get('password');
		$query = DB::table('users')->where('username',$username)->where('password',$password)->get();
		if($query){
			return Redirect::to('search');
		}
		else{
			return Redirect::to('/');
		}
	}
	public function searchBook(){		
		//search at Search Module
		$booktitle = Input::get('booktitle');
		$search = DB::table('books')->where('title','LIKE','%'.$booktitle.'%')
		->orWhere('author','LIKE','%'.$booktitle.'%')
		->orWhere('publisher','LIKE','%'.$booktitle.'%')
		->get();
		
		return View::make('results')->with('results',$search);
	}

	public function OPACsearchBook(){		
		//search at Search Module
		$booktitle = Input::get('booktitle');
		$search = DB::table('books')->where('title','LIKE','%'.$booktitle.'%')
		->orWhere('author','LIKE','%'.$booktitle.'%')
		->orWhere('publisher','LIKE','%'.$booktitle.'%')
		->get();
		
		return View::make('opacresults')->with('results',$search);
	}
	public function OPACreservationSearch(){
		//Search at Reservation Module
		$booktitle = Input::get('booktitle');
		$search = DB::table('books')->where('title','LIKE','%'.$booktitle.'%')->get();
		return View::make('opacreservationsearch')->with('results',$search);	
	}
	public function OPACreserveBook($number){
		//Reserve Borrower Here
		$bookReserve = DB::table('books')->where('number',$number)->first();
		return View::make('opacreservebook')->with('bookReserve',$bookReserve);
	}
	public function OPACsearchBorrowerReserve(){
		//search at Reserve Module Pre-Finalization
		$booknumber = Input::get('booknumber');
		$borrower = Input::get('borrowersname');
		$bookReserve = DB::table('books')->where('number',$booknumber)->first();
		$results = DB::table('borrowers')->where('lname','LIKE','%'.$borrower.'%')->orWhere('fname','LIKE','%'.$borrower.'%')->get();
		return View::make('opacreserveBookFinal')->with('results',$results)->with('bookReserve',$bookReserve);
	}
	public function OPACmakeReserve(){
		//Creates Final Reservation Ticket
		$booknumber = Input::get('number');
		$date_needed = Input::get('dateneeded');
		$borrowernumber = Input::get('borrowernumber');
		$condition = DB::table('reservations')->where('booknumber',$booknumber)
											  ->where('borrowernumber',$borrowernumber)
											  ->count();
		if($condition >= 1){
		return Redirect::to('opacreservation')->with('error',"Duplicate Reservation!");
		} else {
			DB::table('reservations')->insert(
			    array(
			    		'booknumber' => $booknumber,
			    		'date_needed' => $date_needed,
						'borrowernumber' =>	$borrowernumber,
			    	)
			);
		return Redirect::to('opacreservation')->with('error',"Reservation Created!");
		}
	}
	public function searchBookEdit(){
		//search at Books Module
		$booktitle = Input::get('booktitle');
		$search = DB::table('books')->where('title','LIKE','%'.$booktitle.'%')->get();
		return View::make('booksSearch')->with('results',$search);	
	}
	public function searchArchives(){
		$booktitle = Input::get('booktitle');
		$search = DB::table('books')->where('title','LIKE','%'.$booktitle.'%')->get();
		return View::make('archivesSearch')->with('results',$search);	
	}
	public function reservationSearch(){
		//Search at Reservation Module
		$booktitle = Input::get('booktitle');
		$search = DB::table('books')->where('title','LIKE','%'.$booktitle.'%')->get();
		return View::make('reservationSearch')->with('results',$search);	
	}
	
	public function borrowreturnSearch(){
		//Search at Borrow and Return Module
		$booktitle = Input::get('booktitle');
		$search = DB::table('books')->where('title','LIKE','%'.$booktitle.'%')->get();
		return View::make('borrowreturnSearch')->with('results',$search);	
	}

	public function reserveBook($number){
		//Reserve Borrower Here
		$bookReserve = DB::table('books')->where('number',$number)->first();
		return View::make('reserveBook')->with('bookReserve',$bookReserve);
	}

	public function borrowBook($number){
		//Borrow Borrower Here
		$bookBorrow = DB::table('books')->where('number',$number)->first();
		return View::make('borrowBook')->with('bookBorrow',$bookBorrow);		
	}

	public function makeReserve(){
		//Creates Final Reservation Ticket
		$booknumber = Input::get('number');
		$date_needed = Input::get('dateneeded');
		$borrowernumber = Input::get('borrowernumber');
		$condition = DB::table('reservations')->where('booknumber',$booknumber)
											  ->where('borrowernumber',$borrowernumber)
											  ->count();
		if($condition >= 1){
		return Redirect::to('reservationlist')->with('error',"Duplication Reservation!");
		} else {
			DB::table('reservations')->insert(
			    array(
			    		'booknumber' => $booknumber,
			    		'date_needed' => $date_needed,
						'borrowernumber' =>	$borrowernumber,
			    	)
			);
		return Redirect::to('reservationlist');
		}
	}

	public function makeBorrow(){
		$booknumber = Input::get('number');
		$date_borrowed = Input::get('dateborrowed');
		$borrowernumber = Input::get('borrowernumber');
		$expected_return_date = date('Y-m-d',strtotime($date_borrowed . "+2 days"));

			DB::table('borrowreturns')->insert(
				array(
						'booknumber' => $booknumber,
			    		'date_borrowed' => $date_borrowed,
						'borrowernumber' =>	$borrowernumber,
						'expected_return_date' => $expected_return_date,
					)
			);
		DB::table('books')->where('number',$booknumber)->decrement('quantity');
		return Redirect::to('borrowreturnlist');
	}

	public function makeArchive($number){
		//Transfers Book to Archive DB
		DB::table('books')
            ->where('number', $number)
            ->update(
	            	array(
	            		'archive_status' => 'archived',
	            	)
            );
        return Redirect::to('books');
	}
	public function deleteReserve($number){
		//Deletes a Reservation
		DB::table('reservations')->where('number',$number)->delete();
		return Redirect::to('reservationlist');
	}

	public function editBook($number){
		//Edit at Books Module
		$bookEdit = DB::table('books')->where('number',$number)->first();
		return View::make('editBook')->with('bookEdit',$bookEdit);
	}

	public function searchBorrower(){
		//search at Borrower Module
		$borrower = Input::get('borrowersname');
		$results = DB::table('borrowers')->where('lname','LIKE','%'.$borrower.'%')->orWhere('fname','LIKE','%'.$borrower.'%')->get();
		return View::make('borrowersSearch')->with('results',$results);
	}

	public function searchBorrowerReserve(){
		//search at Reserve Module Pre-Finalization
		$booknumber = Input::get('booknumber');
		$borrower = Input::get('borrowersname');
		$bookReserve = DB::table('books')->where('number',$booknumber)->first();
		$results = DB::table('borrowers')->where('lname','LIKE','%'.$borrower.'%')->orWhere('fname','LIKE','%'.$borrower.'%')->get();
		return View::make('reserveBookFinal')->with('results',$results)->with('bookReserve',$bookReserve);
	}

	public function searchBorrowBook(){
		//search at Borrow Module Pre-Finalization
		$booknumber = Input::get('booknumber');
		$borrower = Input::get('borrowersname');
		$bookBorrow = DB::table('books')->where('number',$booknumber)->first();
		$results = DB::table('borrowers')->where('lname','LIKE','%'.$borrower.'%')->orWhere('fname','LIKE','%'.$borrower.'%')->get();
		return View::make('borrowBookFinal')->with('results',$results)->with('bookBorrow',$bookBorrow);
	}

	public function addBorrower(){
		//Input Data
		$fname = Input::get('fname');
		$lname = Input::get('lname');
		$type = Input::get('type');

		$address = Input::get('address');
		$dept = Input::get('department');
		$sect = Input::get('section');
		$yr = Input::get('year');
		$no = Input::get('contact');
		//Insert DB
		DB::table('borrowers')->insert(
			array(
				'fname' => $fname,
				'lname' => $lname,
				'type' => $type,
				'address' => $address,
				'section' => $sect,
				'year' => $yr,
				'contact' => $no,
			)
		);
		return Redirect::to('borrowers');
	}

	public function insertBook(){
		//Input Data
		$title = Input::get('title');
		$author = Input::get('author');
		$publisher = Input::get('publisher');
		$quantity = Input::get('quantity');
		$genre = Input::get('genre');
		$date = Input::get('date');

		$callnumber = Input::get('callnumber');
		$subauthor = Input::get('subauthor');
		$description = Input::get('description');
		$price = Input::get('price');
		//Insert Book Data
		DB::table('books')->insert(
		    array(
		    		'title' => $title,
            		'author' => $author,
            		'publisher' => $publisher,
            		'quantity' => $quantity,
            		'genre' => $genre,
            		'date' => $date,
            		'callnumber' => $callnumber,
            		'subauthor' => $subauthor,
            		'description' => $description,
            		'price' => $price,
		    	)
		);
		return View::make('books');
	}

	public function updateBook(){
		//Input Datas
		$number = Input::get('number');
		$title = Input::get('title');
		$author = Input::get('author');
		$publisher = Input::get('publisher');
		$quantity = Input::get('quantity');
		$genre = Input::get('genre');
		$date = Input::get('date');

		$callnumber = Input::get('callnumber');
		$subauthor = Input::get('subauthor');
		$description = Input::get('description');
		$price = Input::get('price');
		//Update Book
		DB::table('books')
            ->where('number', $number)
            ->update(
	            	array(
	            		'title' => $title,
	            		'author' => $author,
	            		'publisher' => $publisher,
	            		'quantity' => $quantity,
	            		'genre' => $genre,
	            		'date' => $date,
	            		'callnumber' => $callnumber,
	            		'subauthor' => $subauthor,
	            		'description' => $description,
	            		'price' => $price,
	            	)
            );
        return Redirect::to('books');
	}
	
	public function deleteBook($number){
		//Delete Record/Bok
		DB::table('books')->where('number',$number)->delete();
		return Redirect::to('books');
	}

	public function deleteArchive($number){
		//Delete Record/Bok
		DB::table('books')->where('number',$number)->delete();
		return Redirect::to('archives');
	}
	
	public function deleteTransaction($number){
		$data=DB::table('borrowreturns')->where('number',$number)->pluck('booknumber');
		DB::table('books')->where('number',$data)->increment('quantity');
		DB::table('borrowreturns')->where('number',$number)->delete();
		return Redirect::to('borrowreturnlist');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
