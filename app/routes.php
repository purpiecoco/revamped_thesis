<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Views*/
Route::get('opac', function(){
	return View::make('layout2');
});

Route::get('opacsearch',function(){
	return View::make('opacsearch');
});
Route::get('opacreservation', function(){
	return View::make('opacreservation');
});
Route::post('opacresearch', 'MainController@OPACreservationSearch');
Route::get('opacreserveBook/{number}',"MainController@OPACreserveBook");
Route::post('opacreserveBook/searchBorrowerReserve',"MainController@OPACsearchBorrowerReserve");
Route::post('opacreserveBook/makeReserve',"MainController@OPACmakeReserve");

Route::get('/', function()
{
	return View::make('home');
});
Route::get('search', function(){
	 //for datalist item completion
	return View::make('search');
});
Route::get('books', function(){
	return View::make('books');
});
Route::get('borrowers', function(){
	return View::make('borrowers');
});
Route::get('addBorrow', function(){
	return View::make('addBorrower');
});
Route::get('reservation', function(){
	return View::make('reservation');
});
Route::get('acquisition', function(){
	return View::make('acquisition');
});
Route::get('reservationlist',function(){
	return View::make('reservationlist');
});
Route::get('borrowreturn', function(){
	return View::make('borrowreturn');
});
Route::get('borrowreturnlist', function(){
	return View::make('borrowreturnList');
});
Route::get('archives', function(){
	return View::make('archives');
});
Route::get('attendance', function(){

});
/*Test Route*/

Route::get('test',function(){
	
	/*return View::make('test');*/
});

/*Controllers*/
Route::post('opacSearchBook', "MainController@OPACsearchBook"); // opac search

Route::post('login', "MainController@login"); //Login

Route::post('searchBook',"MainController@searchBook"); //Search Route
Route::post('searchBookEdit',"MainController@searchBookEdit"); //Search at Edit Book Route

Route::get('editBook/{number}',"MainController@editBook"); // Edit Book Route Own Page
Route::get('editBook/deleteBook/{number}',"MainController@deleteBook"); //Delete Book Route

Route::post('editBook/updateBook',"MainController@updateBook"); //Update Book Route
Route::post('insertBook',"MainController@insertBook"); //Insert Book Route

Route::post('addBorrower',"MainController@addBorrower");//Add Borrower
Route::post('searchBorrower',"MainController@searchBorrower"); //SearchBorrower route

Route::post('reservationSearch',"MainController@reservationSearch"); //Reservation Search Result Route
Route::get('reserveBook/{number}',"MainController@reserveBook"); //Reserve Selected Book
Route::post('reserveBook/searchBorrowerReserve',"MainController@searchBorrowerReserve"); //Pre-Finalization of Reservation
Route::post('reserveBook/makeReserve',"MainController@makeReserve"); // Creates the Actual Reservation
Route::get('deleteReserve/{number}',"MainController@deleteReserve"); // Deletes a Reservation

Route::post('borrowreturnSearch',"MainController@borrowreturnSearch"); // B&R Search Result Route
Route::get('borrowBook/{number}',"MainController@borrowBook"); //Borrow Selected Book
Route::post('borrowBook/searchBorrowBook',"MainController@searchBorrowBook"); // search for borrowers at borrow module
Route::post('borrowBook/makeBorrow',"MainController@makeBorrow"); // Creates the Actual Borrowing. Add to DB

Route::get('returnBorrow/{number}',"MainController@deleteTransaction"); // Pays and or Delete Transaction

Route::get('makeArchive/{number}',"MainController@makeArchive"); //Transfers book to archive
Route::post('searchArchives',"MainController@searchArchives"); //Search Archives
Route::get('deleteArchive/{number}',"MainController@deleteArchive"); //Deletes book from DB