@extends('layout1')

@section('content')
	<div class="list">
		<form method="post" action="insertBook" name="myForm" onsubmit="return validateForm()">
						<label class="item item-input item-stacked-label">
						    	<span class="input-label">Call Number</span>
						    	<input id="title" name="callnumber" type="text" placeholder="Call Number" required>
					 	</label>
						<label class="item item-input item-stacked-label">
						    	<span class="input-label">Title</span>
						    	<input id="title" name="title" type="text" placeholder="Book Title" required>
					 	</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Author</span>
							    <input id="auth" name="author" type="text" placeholder="Author's Name" required>
						</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Sub - Author</span>
							    <input id="auth" name="subauthor" type="text" placeholder="Sub Author" required>
						</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Publisher</span>
					    		<input id="pub" name="publisher" type="text" placeholder="Publishing Company" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Quantity</span>
					    		<input id="qty"name="quantity" type="number" placeholder="Amount" min="1" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Genre</span>
					    		<input id="gen" name="genre" type="text" placeholder="Book Genre" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Price</span>
					    		<input id="gen" name="price" type="text" placeholder="Price" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Date</span>
					    		<input id="date" name="date" type="date" placeholder="yyyy-mm-dd" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Description</span>
					    		<input id="gen" name="description" type="text" placeholder="Description" required>
					  	</label>
					  	<br>
					  	<button id="btn1" class="button button-block button-positive">
	  						Add Book
						</button>
		</form>	
	</div>
	<script>
	function validateForm() {
/*    var x = document.forms["myForm"]["title"].value;
    var y = document.forms["myForm"]["auth"].value;
    var z = document.forms["myForm"]["pub"].value;
    var a = document.forms["myForm"]["qty"].value;
    var b = document.forms["myForm"]["gen"].value;
    var c = document.forms["myForm"]["date"].value;

	    if (x == null || x == "" || y == null || y == "" || z == null || z == "" || a == null || a == "" || b == null || b == "" || c == null || c == "") {
	        alert("Please provide complete information!");
	        return false;
	    }

	}*/
	</script>
@stop