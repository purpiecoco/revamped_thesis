@extends('layout1')

@section('content')
		<div class="list">
			<?php 
				$borrowreturns = DB::table('borrowreturns')->paginate(10);
			?>
		  	  	<div class="item item-divider">
			    	Transaction List:
			 	</div>
			 	@foreach($borrowreturns as $borrowreturn)
				 	<div class="item">
				 		Transaction Number: {{$borrowreturn->number}}
				    	<?php
				    	$datetime1 = new DateTime($borrowreturn->expected_return_date);
				    	$datetime2 = new DateTime('NOW');
						$interval = $datetime1->diff($datetime2);
				    	$penalty = "Php".$interval->days * 5.00;
				    	if($datetime1 < $datetime2){
				    	print "<b style='float:right;'>Penalty:" . $penalty .".00</b>";	
				    	}
				    	?>
				    	<br>Book Title: <b>{{DB::table('books')->where('number',$borrowreturn->booknumber)->pluck('title')}}</b>
				    	<button id="btn/{{$borrowreturn->number}}" class="button button-positive" style="float:right" onclick="confirm({{$borrowreturn->number}})">
				    		Return this book?
				    	</button>
				    	<a href="returnBorrow/{{$borrowreturn->number}}">
				    	<button id="{{$borrowreturn->number}}"class="button button-assertive" style="float:right;visibility:hidden;">
				    		Click this button to confirm Returning!
				    	</button>
				    	</a>
				    	<br>Borrower's Name: {{DB::table('borrowers')->where('number',$borrowreturn->borrowernumber)->pluck('fname')}} {{DB::table('borrowers')->where('number',$borrowreturn->borrowernumber)->pluck('lname')}}
				    	<br>Date Borrowed: {{$borrowreturn->date_borrowed}}
				    	<br>Expected Date of Return: {{$borrowreturn->expected_return_date}} <br>
				 	</div>
			 	@endforeach
			 	<div class="item item-divider">
			    <div><?php echo $borrowreturns->links(); ?></div>
			 	</div>
		</div>
		<script>
		function confirm(number){
			var button = "btn/"+number;
			document.getElementById(button).style.display = "none";
			document.getElementById(number).style.visibility = "";
		}
		</script>
@stop