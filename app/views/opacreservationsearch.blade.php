@extends('layout2')

@section('content')
		<div class="list">
			
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="opacresearch">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$books = DB::table('books')->get(); //for datalist completion
		      			?>
		      			@foreach($books as $book)
		      				<option>{{$book->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Book List (for Reservation):
			 	</div>
			 	@foreach($results as $result)
				 	<div class="item">
				    	Title: <b>{{$result->title}}</b> <font style="float:right;">Quantity: {{$result->quantity}}</font><br>
				    	Author: {{$result->author}}  <?php 
				    	if($result->quantity > 0){
				    		print "<font color='green' style='float:right;'>Available</font>";
				    	}
				    	else{
				    		print "<font color='red' style='float:right;'>Unavailable</font>";
				    	}
				    	?>
				    	<br>Publisher: {{$result->publisher}}
				    	<a href="opacreserveBook/{{$result->number}}">
				    	<button  class="button button-positive" style="float:right">
				    		Reserve
				    	</button>
				    	</a>
				    	<br>Genre: {{$result->genre}}
				    	<br>Date Added: {{$result->date}}
				 	</div>
			 	@endforeach
		</div>
@stop