@extends('layout1')

@section('content')
		<div class="list">
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="searchBookEdit">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$books = DB::table('books')->get(); //for datalist completion
		      			?>
		      			@foreach($books as $book)
		      				<option>{{$book->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Book List:
			 	</div>
			 	@foreach($books as $book)
				 	<div class="item">
				 		Book Number: {{$book->number}} <br/>
				 		Call Number: {{$book->callnumber}} <br/>
				    	Title: <b>{{$book->title}}</b> <font style="float:right;">Quantity: {{$book->quantity}}</font><br>
				    	Author: {{$book->author}} <br>
				    	Sub Author: {{$book->subauthor}}  <?php 
				    	if($book->quantity > 0){
				    		print "<font color='green' style='float:right;'>Available</font>";
				    	}
				    	else{
				    		print "<font color='red' style='float:right;'>Unavailable</font>";
				    	}
				    	?>
				    	<br>Publisher: {{$book->publisher}}
				    	<a href="editBook/{{$book->number}}">
				    	<button class="button button-positive" style="float:right">
				    		Edit
				    	</button>
				    	</a>
				    	<?php
				    	//Adding to catalogue
				    	$datetime1 = new DateTime($book->date);
				    	$datetime2 = new DateTime('NOW');
				    	$interval = $datetime1->diff($datetime2);
				    	if($interval->y > 4){
				    		if($book->archive_status != 'archived'){
				    	print "<a href=makeArchive/$book->number><button class=button button-positive style=float:right>Eligible for Archiving</button></a>";
				    		}
				    	}
				    	?>
				    	<br>Genre: {{$book->genre}}
				    	<br>Date Added: {{$book->date}}
				    	<br>Price: {{$book->price}}
				    	<br>Description: {{$book->description}}
				 	</div>
			 	@endforeach
		</div>
@stop