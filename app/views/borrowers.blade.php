@extends('layout1')

@section('content')
	<div class="list">
		<a href="/addBorrow">
				<button  class="button button-block button-stable">
						Add Borrowers
				</button>
		</a>
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="searchBorrower">
		      		<input type="text" list="borrowerslist" name="borrowersname" placeholder="Enter Surname" autocomplete="off" required>
		      		<datalist id="borrowerslist">
		      			<?php 
		      				$borrowers = DB::table('borrowers')->get(); //for datalist completion
		      			?>
		      			@foreach($borrowers as $borrower)
		      				<option>{{$borrower->lname}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Borrowers List:
			 	</div>
			 	@foreach($borrowers as $borrower)
				 	<div class="item">
				 		Borrower's Number: {{$borrower->number}}<br>
				    	Borrower's Name: <b>{{$borrower->fname}}</b> <b>{{$borrower->lname}}</b> 
				    	<br/>Type: {{$borrower->type}}
				    	<br>
				    	Contact Detail: {{$borrower->contact}}<br>
				    	Address: {{$borrower->address}}<br>
				    	Department: {{$borrower->department}}<br>
				    	Section: {{$borrower->section}}<br>
				    	Year: {{$borrower->year}}<br>


				 	</div>
			 	@endforeach
			 	
		</div>
@stop