@extends('layout2')

@section('content')
		<div class="list">

		  	<div class="item item-input-inset">

		    	<label class="item-input-wrapper">
		  		<form method="post" action="opacresearch">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$books = DB::table('books')->get(); //for datalist completion
		      			?>
		      			@foreach($books as $book)
		      				<option>{{$book->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>
				@if(Session::get('error'))
			<div class="card">
			  <div class="item item-text-wrap">
			    {{Session::get('error')}}
			  </div>
			</div>
			@endif
		  	  	<div class="item item-divider">
			    	Book List (for Reservation):
			 	</div>
			 	@foreach($books as $book)
				 	<div class="item">
				    	Title: <b>{{$book->title}}</b> <font style="float:right;">Quantity: {{$book->quantity}}</font><br>
				    	Author: {{$book->author}}  <?php 
				    	if($book->quantity > 0){
				    		print "<font color='green' style='float:right;'>Available</font>";
				    	}
				    	else{
				    		print "<font color='red' style='float:right;'>Unavailable</font>";
				    	}
				    	?>
				    	<br>Publisher: {{$book->publisher}}
				    	<a href="opacreserveBook/{{$book->number}}">
				    	<button  class="button button-positive" style="float:right">
				    		Reserve
				    	</button>
				    	</a>
				    	<br>Genre: {{$book->genre}}
				    	<br>Date Added: {{$book->date}}
				 	</div>
			 	@endforeach
		</div>
@stop