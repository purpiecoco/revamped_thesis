@extends('layout1')

@section('content')
		<div class="list">
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="searchArchives">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$books = DB::table('books')->where('archive_status','archived')->get(); //for datalist completion
		      			?>
		      			@foreach($books as $book)
		      				<option>{{$book->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Archives List:
			 	</div>
			 	@foreach($books as $book)
				 	<div class="item">
				 		Archive Status: <b style="color:red;">{{$book->archive_status}}</b><br>
				    	Title: <b>{{$book->title}}</b> <font style="float:right;">Quantity: {{$book->quantity}}</font><br>
				    	Author: {{$book->author}}  <?php 
				    	if($book->quantity > 0){
				    		print "<font color='green' style='float:right;'>Available</font>";
				    	}
				    	else{
				    		print "<font color='red' style='float:right;'>Unavailable</font>";
				    	}
				    	?>
				    	<br>Publisher: {{$book->publisher}}
				    	<button id="btn/{{$book->number}}" class="button button-positive" style="float:right" onclick="confirm({{$book->number}})">
				    		Delete from Archives?
				    	</button>
				    	<a href="deleteArchive/{{$book->number}}">
				    	<button id="{{$book->number}}" class="button button-assertive" style="float:right;visibility:hidden;">
				    		Click this button to confim deletion!
				    	</button>
				    	</a>
				    	
				    	<br>Genre: {{$book->genre}}
				    	<br>Date Added: {{$book->date}}
				 	</div>
			 	@endforeach
		</div>
		<script>
		function confirm(number){
			var button = "btn/"+number;
			document.getElementById(button).style.display = "none";
			document.getElementById(number).style.visibility = "";
		}
		</script>
@stop