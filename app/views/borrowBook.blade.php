@extends('layout')

@section('content')
	<div class="bar bar-header bar-dark">
  	<h1 class="title">San Diego Parochioal School Library System - High School Department</h1>
	</div>
		<br><br>
		<div class="card">
						<div class="item item-divider">
						    <center>Borrow Book : {{$bookBorrow->title}} - {{$bookBorrow->author}}</center>
						</div>
			<div class="item item-text-wrap">		
				<div class="list">
					<div class="item item-divider">
					    Book Information
					</div>
					<!-- <form method="post" action="reserveBook"> -->
						<input name="number" type="hidden" value="{{$bookBorrow->number}}">
						<label class="item item-input item-stacked-label">
						    	<span class="input-label">Title</span>
						    	<input name="title" type="text" placeholder="Book Title" value="{{$bookBorrow->title}}"  disabled>
					 	</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Author</span>
							    <input name="author" type="text" placeholder="Author's Name" value="{{$bookBorrow->author}}"  disabled>
						</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Publisher</span>
					    		<input name="publisher" type="text" placeholder="Publishing Company" value="{{$bookBorrow->publisher}}"  disabled>
					  	</label>
					 	<!-- Borrower Code Block -->
						<?php 
		      				$borrowers = DB::table('borrowers')->get(); //for datalist completion
		      			?>
						<div class="item item-divider">
							    Select Borrower
						</div>
						<div class="item item-input-inset">
						    	<label class="item-input-wrapper">
						  		<form method="post" action="searchBorrowBook">
						  			<input type="hidden" name="booknumber" value="{{$bookBorrow->number}}"/>
						      		<input type="text" list="borrowerslist" name="borrowersname" placeholder="Enter Surname" autocomplete="off">
						      		<datalist id="borrowerslist">
						      			<?php 
						      				$borrowers = DB::table('borrowers')->get(); //for datalist completion
						      			?>
						      			@foreach($borrowers as $borrower)
						      				<option>{{$borrower->fname}}</option>
						      			@endforeach
						      		</datalist>
						  		</form>	
						    	</label>
		  				</div>
					<!-- </form> -->
				</div>
				<!-- ListDividerHere -->
			</div>
			<!-- <center>
				  	<div class="item item-divider">
						<a href="deleteBook/{{$bookBorrow->number}}">
						  	<button class="button button-block button-assertive">
		  						Delete
							</button>
						</a>
					</div>
			</center> -->
		</div>
@stop