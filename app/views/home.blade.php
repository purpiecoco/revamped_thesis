@extends('layout')

@section('content')
	<div class="bar bar-header bar-dark">
  	<h1 class="title">San Diego Parochioal School Library System - High School Department</h1>
	</div>
	<hr>
	<br>
	<br>
	<div class="card">
	  <div class="item item-divider">
	    Login Form
	  </div>
	  <div class="item item-text-wrap">
	    <form method=POST action='login'>
			Username: <input type='text' name='username' placeholder='username'/><br>
			Password: <input type='password' name='password' placeholder='password'/><br>
			<button class="button button-positive">
				Submit
			</button>
		</form>
	  </div>
	</div>
		
@stop