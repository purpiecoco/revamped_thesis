@extends('layout')

@section('content')
	<div class="bar bar-header bar-dark">
  	<h1 class="title">San Diego Parochioal School Library System - High School Department</h1>
	</div>
		<br><br>
		<div class="card">
						<div class="item item-divider">
						    <center>Reserve Book : {{$bookReserve->title}} - {{$bookReserve->author}}</center>
						</div>
			<div class="item item-text-wrap">		
				<div class="list">
					<div class="item item-divider">
					    Book Information
					</div>
					<!-- <form method="post" action="reserveBook"> -->
						<input name="number" type="hidden" value="{{$bookReserve->number}}">
						<label class="item item-input item-stacked-label">
						    	<span class="input-label">Title</span>
						    	<input name="title" type="text" placeholder="Book Title" value="{{$bookReserve->title}}"  disabled>
					 	</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Author</span>
							    <input name="author" type="text" placeholder="Author's Name" value="{{$bookReserve->author}}"  disabled>
						</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Publisher</span>
					    		<input name="publisher" type="text" placeholder="Publishing Company" value="{{$bookReserve->publisher}}"  disabled>
					  	</label>
					 	<!-- Borrower Code Block -->
						<?php 
		      				$borrowers = DB::table('borrowers')->get(); //for datalist completion
		      			?>
						<div class="item item-divider">
							    Select Borrower
						</div>
						<div class="item item-input-inset">
						    	<label class="item-input-wrapper">
						  		<form method="post" action="searchBorrowerReserve">
						  			<input type="hidden" name="booknumber" value="{{$bookReserve->number}}"/>
						      		<input type="text" list="borrowerslist" name="borrowersname" placeholder="Enter Surname" autocomplete="off">
						      		<datalist id="borrowerslist">
						      			<?php 
						      				$borrowers = DB::table('borrowers')->get(); //for datalist completion
						      			?>
						      			@foreach($borrowers as $borrower)
						      				<option>{{$borrower->lname}}</option>
						      			@endforeach
						      		</datalist>
						  		</form>	
						    	</label>
		  				</div>
					<!-- </form> -->
				</div>
				<!-- ListDividerHere -->
			</div>
			<!-- <center>
				  	<div class="item item-divider">
						<a href="deleteBook/{{$bookReserve->number}}">
						  	<button class="button button-block button-assertive">
		  						Delete
							</button>
						</a>
					</div>
			</center> -->
		</div>
@stop