@extends('layout')

@section('content')
	<div class="bar bar-header bar-dark">
  	<h1 class="title">San Diego Parochioal School Library System - High School Department</h1>
	</div>
		<br><br>
		<div class="card">
						<div class="item item-divider">
						    <center>Edit Book: {{$bookEdit->title}}</center>
						</div>
			<div class="item item-text-wrap">		
				<div class="list">
					<form method="post" action="updateBook">
						<input name="number" type="hidden" value="{{$bookEdit->number}}">
						<label  class="item item-input item-stacked-label">
						    	<span class="input-label">Call Number</span>
						    	<input id="title" name="callnumber" type="text" placeholder="Call Number" value="{{$bookEdit->callnumber}}" required>
					 	</label>
						<label  class="item item-input item-stacked-label">
						    	<span class="input-label">Title</span>
						    	<input id="title" name="title" type="text" placeholder="Book Title" value="{{$bookEdit->title}}" required>
					 	</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Author</span>
							    <input id="auth" name="author" type="text" placeholder="Author's Name" value="{{$bookEdit->author}}" required>
						</label>
						<label  class="item item-input item-stacked-label">
						    	<span class="input-label">Sub Author</span>
						    	<input id="title" name="subauthor" type="text" placeholder="Sub Author" value="{{$bookEdit->subauthor}}" required>
					 	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Publisher</span>
					    		<input id="pub" name="publisher" type="text" placeholder="Publishing Company" value="{{$bookEdit->publisher}}" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Quantity</span>
					    		<input id="qty" name="quantity" type="number" placeholder="Amount" value="{{$bookEdit->quantity}}" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Genre</span>
					    		<input id="gen" name="genre" type="text" placeholder="Book Genre" value="{{$bookEdit->genre}}" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Price</span>
					    		<input id="gen" name="price" type="text" placeholder="Price" value="{{$bookEdit->price}}" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Date</span>
					    		<input id="date" name="date" type="date" placeholder="Book Genre" value="{{$bookEdit->date}}" required>
					  	</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Description</span>
					    		<input id="gen" name="description" type="text" placeholder="Description" value="{{$bookEdit->description}}" required>
					  	</label>
					  	<br>
					  	<button class="button button-block button-positive">
	  						Update Book
						</button>
					</form>
				</div>
			</div>
			<center>
				  	<div class="item item-divider">
				  		<button id="btn/{{$bookEdit->number}}" class="button button-block button-stable" onclick="confirm({{$bookEdit->number}})">
				    		Delete this book?
				    	</button>
						<a href="deleteBook/{{$bookEdit->number}}">
						  	<button id="{{$bookEdit->number}}" class="button button-block button-assertive" style="visibility:hidden;">
		  						Click here again to Delete!
							</button>
						</a>
					</div>
			</center>
		</div>
		<script>
		function validateForm() {
	    var x = document.forms["myForm"]["title"].value;
	    var y = document.forms["myForm"]["auth"].value;
	    var z = document.forms["myForm"]["pub"].value;
	    var a = document.forms["myForm"]["qty"].value;
	    var b = document.forms["myForm"]["gen"].value;
	    var c = document.forms["myForm"]["date"].value;

		    if (x == null || x == "" || y == null || y == "" || z == null || z == "" || a == null || a == "" || b == null || b == "" || c == null || c == "") {
		        alert("Please provide complete information!");
		        return false;
		    }

		}
		function confirm(number){
			var button = "btn/"+number;
			document.getElementById(button).style.display = "none";
			document.getElementById(number).style.visibility = "";
		}
		</script>
@stop