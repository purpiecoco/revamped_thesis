<html>
	<head>
		<title>SDPS Library System</title>
		{{HTML::style('css/ionic.css')}}
		<style type="text/css">
		a{
			text-decoration: none;
		}
		</style>
	</head>
	<body onload="hover()">
		<div class="bar bar-header bar-dark">
		<a href="/search"><button class="button icon ion-home"></button></a>
	  	<h1 class="title">San Diego Parochial School Library System - High School Department</h1>
		</div>
			<br>
			<br>
			<div class="row responsive-md">
		  			<div class="col">
		  				<div class="card">
						  <div class="item item-divider">
						    <center>Menu</center>
						  </div>
						  <div class="item item-text-wrap">
						  	<a href="search">
							    <button id="a1" class="button button-block button-stable">
								  Search
								</button>
							</a>
							<a href="books">
								<button id="a2" class="button button-block button-stable">
								  Books
								</button>
							</a>
							<a href="acquisition">
								<button id="a3" class="button button-block button-stable">
								  Acquisition
								</button>
							</a>
							<a href="borrowers">
								<button id="a4" class="button button-block button-stable">
								  Borrowers
								</button>
							</a>
							<a href="reservation">
								<button id="a5" class="button button-block button-stable">
								  Reservation
								</button>
							</a>
							<a href="borrowreturn">
								<button id="a6" class="button button-block button-stable">
								  Borrowing and Returning
								</button>
							</a>
							<a href="archives">
								<button id="a7" class="button button-block button-stable">
								  Archives
								</button>
							</a>
							<!-- <a href="catalogue">
								<button class="button button-block button-stable">
								  Cataloguing
								</button>
							</a> -->
							<button class="button button-block button-assertive">
							  Logout
							</button>
						  </div>
						</div>
		  			</div>
		  			<div class="col col-75">
		  				<div class="card">
						  <div class="item item-divider">
						    <center>San Diego Library System</center>
						  </div>
						  <div class="item item-text-wrap">
						    @yield('content')
						  </div>
						</div>
		  			</div>
			</div>
	</body>
	<script>
	function hover(){
		var x =	window.location.pathname;
		if(x == '/search' || x == '/searchBook'){
			document.getElementById('a1').className = "button button-block button-positive";
		}
		if(x == '/books' || x == '/searchBookEdit'){
			document.getElementById('a2').className = "button button-block button-positive";
		}
		if(x == '/acquisition'){
			document.getElementById('a3').className = "button button-block button-positive";
		}
		if(x == '/borrowers' || x == '/searchBorrower'){
			document.getElementById('a4').className = "button button-block button-positive";
		}
		if(x == '/reservation' || x == '/reservationSearch'){
			document.getElementById('a5').className = "button button-block button-positive";
		}
		if(x == '/borrowreturn' || x == '/borrowreturnSearch'){
			document.getElementById('a6').className = "button button-block button-positive";
		}
		if(x == '/archives' || x == '/archivesSearch'){
			document.getElementById('a7').className = "button button-block button-positive";
		}
	}
	</script>
</html>