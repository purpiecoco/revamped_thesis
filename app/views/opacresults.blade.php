@extends('layout2')

@section('content')
		<div class="list">
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="opacSearchBook">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$booksearches = DB::table('books')->get(); //for datalist completion
		      			?>
		      			@foreach($booksearches as $booksearch)
		      				<option>{{$booksearch->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Search Results:
			 	</div>
			 	@foreach($results as $result)
				 	<div class="item">
				 		Book Number: {{$result->number}} <br/>
				 		Call Number: {{$result->callnumber}} <br/>
				    	Title: <b>{{$result->title}}</b> <font style="float:right;">Quantity: {{$result->quantity}}</font><br>
				    	Author: {{$result->author}}<br>
				    	Sub Author: {{$result->subauthor}}
				    	 <?php 
				    	if($result->quantity > 0){
				    		print "<font color='green' style='float:right;'>Available</font>";
				    	}
				    	else{
				    		print "<font color='red'>Unavailable</font>";
				    	}
				    	?>
				    	<br>Publisher: {{$result->publisher}}
				    	<br>Genre: {{$result->genre}}
				    	<br>Date Added: {{$result->date}}
				    	<br>Price: {{$result->price}}
				    	<br>Description: {{$result->description}}
				 	</div>
			 	@endforeach
		</div>
@stop