@extends('layout1')
@section('content')
<div class="list">
		<form method="post" action="addBorrower" name="myForm" onsubmit="return validateForm()">
						<label class="item item-input item-stacked-label">
						    	<span class="input-label">First Name</span>
						    	<input id="title" name="fname" type="text" placeholder="First Name" required>
					 	</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Last Name</span>
							    <input id="auth" name="lname" type="text" placeholder="Author's Name" required>
						</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Contact Number</span>
							    <input id="auth" name="contact" type="text" placeholder="Address" required>
						</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Address</span>
							    <input id="auth" name="address" type="text" placeholder="Address" required>
						</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Department</span>
							    <input id="auth" name="department" type="text" placeholder="Address" required>
						</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Section</span>
							    <input id="auth" name="section" type="text" placeholder="Address" required>
						</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Year</span>
							    <input id="auth" name="year" type="text" placeholder="Address" required>
						</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Type</span>
					    		<input id="pub" name="type" type="text" placeholder="Type" required>
					  	</label>
					  	<br>
					  	<button id="btn1" class="button button-block button-positive">
	  						Add Borrower
						</button>
		</form>	
</div>
@stop