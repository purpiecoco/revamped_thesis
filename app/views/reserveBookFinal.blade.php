@extends('layout')

@section('content')
	<div class="bar bar-header bar-dark">
  	<h1 class="title">San Diego Parochioal School Library System - High School Department</h1>
	</div>
		<br><br>
		<div class="card">
						<div class="item item-divider">
						    <center>Reserve Book : {{$bookReserve->title}} - {{$bookReserve->author}}</center>
						</div>
			<div class="item item-text-wrap">		
				<div class="list">
					<div class="item item-divider">
					    Book Information
					</div>
					<!-- <form method="post" action="reserveBook"> -->
						<label class="item item-input item-stacked-label">
						    	<span class="input-label">Title</span>
						    	<input name="title" type="text" placeholder="Book Title" value="{{$bookReserve->title}}"  disabled>
					 	</label>
						<label class="item item-input item-stacked-label">
							    <span class="input-label">Author</span>
							    <input name="author" type="text" placeholder="Author's Name" value="{{$bookReserve->author}}"  disabled>
						</label>
					  	<label class="item item-input item-stacked-label">
					    		<span class="input-label">Publisher</span>
					    		<input name="publisher" type="text" placeholder="Publishing Company" value="{{$bookReserve->publisher}}"  disabled>
					  	</label>
						<form method="post" action="makeReserve" onsubmit="return validateForm()">
						<input name="number" type="hidden" value="{{$bookReserve->number}}">
						  	<!-- Select Date Block -->
						 	<label class="item item-input item-stacked-label">
						    		<span class="input-label">Date (yyyy-mm-dd)</span>
						    		<input id="date" name="dateneeded" type="date" placeholder="yyyy-mm-dd" required>
						  	</label>
						 	<!-- Borrower Code Block -->
							<?php 
			      				$borrowers = DB::table('borrowers')->get(); //for datalist completion
			      			?>
							<div class="item item-divider">
								    Select Borrower from search Results
							</div>
								@foreach($results as $result)
								 	<div class="item">
								 		<input type="radio" name="borrowernumber" value="{{$result->number}}" required>
								 		Borrower's Number: {{$result->number}}<br>
								 		<!-- <a href="reserveTicket/{{$bookReserve->number}}/{{$result->number}}">
								    	<button  class="button button-positive" style="float:right">
								    		Reserve for this Borrower
								    	</button>
								    	</a> -->
								    	Borrower's Name: <b>{{$result->fname}}</b> <b>{{$result->lname}}</b> 
								    	<br/>Type: {{$result->type}}
								 	</div>
							 	@endforeach
							<div class="item item-divider">
							   	<button  class="button button-block button-positive">
							    	Create Reservation
							   	</button>
							</div>
						</form>
				</div>
			</div>
			
		</div>
	<script>
	function validateForm() {
    var x = document.forms["myForm"]["date"].value;
    /*
    var y = document.forms["myForm"]["auth"].value;
    var z = document.forms["myForm"]["pub"].value;
    var a = document.forms["myForm"]["qty"].value;
    var b = document.forms["myForm"]["gen"].value;
    var c = document.forms["myForm"]["date"].value;

    if (x == null || x == "" || y == null || y == "" || z == null || z == "" || a == null || a == "" || b == null || b == "" || c == null || c == "") {
        alert("Please provide complete information!");
        return false;
    }
*/	
		if (x == null || x == ""){
			alert("Please enter the date borrowed!");
			return false;
		}
	}
	</script>
@stop