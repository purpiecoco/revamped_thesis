@extends('layout1')

@section('content')
	<div class="list">
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="searchBorrower">
		      		<input type="text" list="borrowerslist" name="borrowersname" placeholder="Enter Surname" autocomplete="off" required>
		      		<datalist id="borrowerslist">
		      			<?php 
		      				$borrowers = DB::table('borrowers')->get(); //for datalist completion
		      			?>
		      			@foreach($borrowers as $borrower)
		      				<option>{{$borrower->lname}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Borrowers List:
			 	</div>
			 	@foreach($results as $result)
				 	<div class="item">
				 		Borrower's Number: {{$result->number}}<br>
				    	Borrower's Name: <b>{{$result->fname}}</b> <b>{{$result->lname}}</b> 
				    	<br>Type: {{$result->type}}
				    	<br>
				    	Contact Detail: {{$result->contact}}<br>
				    	Address: {{$result->address}}<br>
				    	Department: {{$result->department}}<br>
				    	Section: {{$result->section}}<br>
				    	Year: {{$result->year}}<br>
				 	</div>
			 	@endforeach
		</div>
@stop