@extends('layout1')

@section('content')
		<div class="list">
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="searchBookEdit">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$books = DB::table('books')->get(); //for datalist completion
		      			?>
		      			@foreach($books as $book)
		      				<option>{{$book->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Book List:
			 	</div>
			 	@foreach($results as $result)
				 	<div class="item">
				 		Book Number: {{$result->number}} <br/>
				 		Call Number: {{$result->callnumber}} <br/>
				    	Title: <b>{{$result->title}}</b> <font style="float:right;">Quantity: {{$result->quantity}}</font><br>
				    	Author: {{$result->author}}<br>
				    	Sub Author: {{$book->subauthor}}  <?php 
				    	if($result->quantity > 0){
				    		print "<font color='green' style='float:right;'>Available</font>";
				    	}
				    	else{
				    		print "<font color='red' style='float:right;'>Unavailable</font>";
				    	}
				    	?>
				    	<br>Publisher: {{$result->publisher}}
				    	<a href="editBook/{{$result->number}}">
				    	<button  class="button button-positive" style="float:right">
				    		Edit
				    	</button>
				    	</a>
				    	<?php
				    	//Adding to catalogue
				    	$datetime1 = new DateTime($result->date);
				    	$datetime2 = new DateTime('NOW');
				    	$interval = $datetime1->diff($datetime2);
				    	if($interval->y > 4){
				    		if($book->archive_status != 'archived'){
				    	print "<a href=makeArchive/$result->number><button class=button button-positive style=float:right>Eligible for Archiving</button></a>";
				    		}
				    	}
				    	?>
				    	<br>Genre: {{$result->genre}}
				    	<br>Date Added: {{$result->date}}
				    	<br>Price: {{$result->price}}
				    	<br>Description: {{$result->description}}
				 	</div>
			 	@endforeach
		</div>
@stop