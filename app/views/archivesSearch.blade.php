@extends('layout1')

@section('content')
		<div class="list">
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="searchArchives">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$books = DB::table('books')->where('archive_status','archived')->get(); //for datalist completion
		      			?>
		      			@foreach($books as $book)
		      				<option>{{$book->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Archives List:
			 	</div>
			 	@foreach($results as $result)
				 	<div class="item">
				 		Archive Status: <b style="color:red;">{{$result->archive_status}}</b><br>
				    	Title: <b>{{$result->title}}</b> <font style="float:right;">Quantity: {{$result->quantity}}</font><br>
				    	Author: {{$result->author}}  <?php 
				    	if($result->quantity > 0){
				    		print "<font color='green' style='float:right;'>Available</font>";
				    	}
				    	else{
				    		print "<font color='red' style='float:right;'>Unavailable</font>";
				    	}
				    	?>
				    	<br>Publisher: {{$result->publisher}}
				    	<button id="btn/{{$result->number}}" class="button button-positive" style="float:right" onclick="confirm({{$result->number}})">
				    		Delete from Archives?
				    	</button>
				    	<a href="deleteArchive/{{$result->number}}">
				    	<button id="{{$result->number}}" class="button button-assertive" style="float:right;visibility:hidden;">
				    		Click this button to confim deletion!
				    	</button>
				    	</a>
				    	<br>Genre: {{$result->genre}}
				    	<br>Date Added: {{$result->date}}
				 	</div>
			 	@endforeach
		</div>
		<script>
		function confirm(number){
			var button = "btn/"+number;
			document.getElementById(button).style.display = "none";
			document.getElementById(number).style.visibility = "";
		}
		</script>
@stop