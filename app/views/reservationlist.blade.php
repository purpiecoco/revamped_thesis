@extends('layout1')

@section('content')
		<div class="list">
			<?php 
				$reservations = DB::table('reservations')->paginate(10);
			?>
			@if(Session::get('error'))
			<div class="card">
			  <div class="item item-text-wrap assertive">
			    Duplicate Data Entries!
			  </div>
			</div>
			@endif
		  	  	<div class="item item-divider">
			    	Reservation List:
			 	</div>
			 	@foreach($reservations as $reservation)
				 	<div class="item">
				 		Reservation Number: {{$reservation->number}}
				    	<br>Book Title: <b>{{DB::table('books')->where('number',$reservation->booknumber)->pluck('title')}}</b>
				    	<button id="btn/{{$reservation->number}}" class="button button-positive" style="float:right" onclick="confirm({{$reservation->number}})">
				    		Delete this Reservation?
				    	</button>
				    	<a href="deleteReserve/{{$reservation->number}}">
				    	<button id="{{$reservation->number}}" class="button button-assertive" style="float:right;visibility:hidden;">
				    		Click here again to confirm deletion!
				    	</button>
				    	</a>
				    	<br>Borrower's Name: {{DB::table('borrowers')->where('number',$reservation->borrowernumber)->pluck('fname')}} {{DB::table('borrowers')->where('number',$reservation->borrowernumber)->pluck('lname')}}
				    	<br>Date Needed: {{$reservation->date_needed}}
				 	</div>
			 	@endforeach
			 	<div class="item item-divider">
			    <div><?php echo $reservations->links(); ?></div>
			 	</div>
		</div>
		<script>
		function confirm(number){
			var button = "btn/"+number;
			document.getElementById(button).style.display = "none";
			document.getElementById(number).style.visibility = "";
		}
		</script>
@stop