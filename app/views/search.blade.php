@extends('layout1')

@section('content')
		<div class="list">
		  	<div class="item item-input-inset">
		    	<label class="item-input-wrapper">
		  		<form method="post" action="searchBook">
		      		<input type="text" list="booklist" name="booktitle" placeholder="Enter Book Title" autocomplete="off" required>
		      		<datalist id="booklist">
		      			<?php 
		      				$booksearches = DB::table('books')->get(); //for datalist completion
		      			?>
		      			@foreach($booksearches as $booksearch)
		      				<option>{{$booksearch->title}}</option>
		      			@endforeach
		      		</datalist>
		  		</form>	
		    	</label>
		  	</div>

		  	  	<div class="item item-divider">
			    	Search Results:
			 	</div>	
		</div>
@stop